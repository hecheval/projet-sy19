pb_class <- read.table("tp3_a18_clas_app.txt")
pb_reg <- read.table("tp3_a18_reg_app.txt")
cor(pb_reg)

#ensembles d'apprentissage et de test
nreg = dim(pb_reg)[1]

reg.mask = sort(sample(x = 1:nreg, size = trunc(nreg*2/3)))

reg.appr = pb_reg[reg.mask,]
reg.test = pb_reg[-reg.mask,]
x.appr = reg.appr[,1:50]
y.appr = reg.appr[,"y"]

x.test = reg.test[,1:50]
y.test = reg.test[,"y"]


nclass = dim(pb_class)[1]

class.mask = sort(sample(x = 1:nclass, size = trunc(nclass*2/3)))

class.appr = pb_class[class.mask,]
class.test = pb_class[-class.mask,]


#mod�les de r�gression
#r�gression lin�aire

summary(pb_reg)
reg <- lm(y ~ ., data = reg.appr)
summary(reg)
confint(reg)

x11()
plot(reg.appr$y,reg$fitted.values)
abline(0,1)


rres = reg$residuals
rstd = rstandard(reg)
rstu = rstudent(reg)

plot(reg.appr$y,rstd)
plot(reg.appr$y,rstu)

shapiro.test(rres)

#r�sidus en fonction des variables explicatives

attach(reg.appr)
varnames <- attr(reg$terms, "term.labels")
par(mfrow = c(length(varnames), 3))
par(mar=c(1,1,1,1))
for(name in varnames) {
  plot(get(name), rres, xlab = name)
  plot(get(name), rstd, xlab = name)
  plot(get(name), rstu, xlab = name)
}

## Q-Q plots
qqnorm(rres, asp = 1)
qqline(rres, dist = qnorm)

qqnorm(rstd, asp = 1)
qqline(rstd, dist = qnorm)

qqnorm(rstu, asp = 1)
qqline(rstu, dist = qnorm)

#influence globale
plot(reg, which = 4, cook.levels = c(0, 0.1))

plot(reg, which = 5, cook.levels = c(0, 0.1))


reg2 <- lm(y ~ X1 + X2 + X3 + X14 + X19 + X32 + X34 + X35 + X37 + X38 + X39 + X41, data = reg.appr)
(summary(reg2))

x11()
plot(reg.appr$y, reg2$fitted.values)
abline(0,1)

plot(reg2, which = 4, cook.levels = c(0, 0.1))

plot(reg2, which = 5, cook.levels = c(0, 0.1))

#avec les donn�es de test
pred <- predict(reg,newdata = reg.test)

x11()
plot(reg.test$y,pred)

yi = reg.test$y

x11()
plot(yi, pred[, 1], ylim = range(pred), asp = 1)
lines(yi, pred[, 2])
lines(yi, pred[, 3])
abline(0, 1)
mean((y.test-pred)^2)

#stepwise selection
library(leaps)

reg.fit.test<-regsubsets(y ~ .,data=reg.appr,method="exhaustive",nvmax=15)
x11()
plot(reg.fit.f,scale="r2")


#forward
reg.fit.f<-regsubsets(y ~ .,data=reg.appr,method="forward",nvmax=15)
x11()
plot(reg.fit.f,scale="r2", main = "")
title(main ="forward")
summary(reg.fit.f)

#backward
reg.fit.b<-regsubsets(y ~ .,data=reg.appr,method="backward",nvmax=15)
x11()
plot(reg.fit.b,scale="r2", main = "")
title(main = "backward")

#AIC et BIC
reg.fit <- regsubsets(y ~., data = reg.appr, method = "exhaustive", nvmax = 15, really.big = T)
x11()
plot(reg.fit,scale="adjr2", main = "") 
title(main = "AIC")
x11()
plot(reg.fit,scale="bic", main = "")
title(main = "BIC")

reg.model <- lm(y ~ X1 + X2 + X3 + X14 + X17 + X19 + X32 + X34 + X35 + X37 + X38 + X39 + X41 + X46 + X49, data = reg.appr)
summary(reg.model)

confint(reg.model)

x11()
plot(reg.appr$y,reg.model$fitted.values)
abline(0,1)


rres.m = reg.model$residuals
rstd.m = rstandard(reg.model)
rstu.m = rstudent(reg.model)

plot(reg.appr$y,rstd.m)
plot(reg.appr$y,rstu.m)

shapiro.test(rres.m)

## Q-Q plots
qqnorm(rres.m, asp = 1)
qqline(rres.m, dist = qnorm)

qqnorm(rstd.m, asp = 1)
qqline(rstd.m, dist = qnorm)

qqnorm(rstu.m, asp = 1)
qqline(rstu.m, dist = qnorm)

#influence globale
plot(reg.model, which = 4, cook.levels = c(0, 0.1))

plot(reg.model, which = 5, cook.levels = c(0, 0.1)) #point aberrant

cooks.distance(reg.model)

#avec les donn�es de test
pred.m <- predict(reg.model,newdata = reg.test)

x11()
plot(reg.test$y,pred.m)
abline(0,1)


#transfo non lin�aires
# faire des puissances de certaines colonnes
# transfo log qui �crase les donn�es (et donc le bruit)(quand on voit qu'on a un bruit qui augmente)
# transfo exp si on a un bruit qui diminue

plot(reg.appr$X1,rres.m)
plot(reg.appr$X2,rres.m)
plot(reg.appr$X3,rres.m)
plot(reg.appr$X14,rres.m)
plot(reg.appr$X17,rres.m)
plot(reg.appr$X19,rres.m)
plot(reg.appr$X32,rres.m)
plot(reg.appr$X34,rres.m)
plot(reg.appr$X35,rres.m)
plot(reg.appr$X37,rres.m)
plot(reg.appr$X38,rres.m)
plot(reg.appr$X39,rres.m) #diminue?
plot(reg.appr$X41,rres.m)
plot(reg.appr$X46,rres.m)
plot(reg.appr$X49,rres.m) #diminue??


reg.model2 <- lm(y ~ X1 + X2 + X3 + X14 + X17 + X19 + X32 + X34 + X35 + X37 + X38 + exp(X39) + X41 + X46 + (X49)^2, data = reg.appr)
summary(reg.model2)
summary(reg.model)

confint(reg.model2)

x11()
plot(reg.appr$y,reg.model2$fitted.values)
abline(0,1)

## 3

reg.model3 <- lm(y ~ X1 + X2 + X3 + X14 + X19 + X32 + X34 + X35 + X37 + X38 + exp(X39) + X41 + X46 + (X49)^2, data = reg.appr)
summary(reg.model3)
summary(reg.model)

confint(reg.model3)

x11()
plot(reg.appr$y,reg.model3$fitted.values)
abline(0,1)

plot(reg.model3, which = 4, cook.levels = c(0, 0.1))

plot(reg.model3, which = 5, cook.levels = c(0, 0.1))

## 4
reg.model4 <- lm(y ~ X1 + X2 + X3 + X14 + X19 + X32 + X34 + X35 + X37 + X38 + X39 + X41 + X46 + X49, data = reg.appr)
summary(reg.model4)

plot(reg.model4, which = 4, cook.levels = c(0, 0.1))

plot(reg.model4, which = 5, cook.levels = c(0, 0.1))


#### Classifieur ####

class.lda <- lda(y ~ ., data = class.appr)
summary(class.lda)

class.pred <- predict(class.lda, newdata = class.test)
